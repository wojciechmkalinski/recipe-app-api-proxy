# Recipe API app proxy

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8080`)
* `APP_HOST` - host of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
